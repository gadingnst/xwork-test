const { Schema, model } = require('mongoose')

const MusicSchema = new Schema({
    title: String,
    album: String,
    artist: String,
    year: Schema.Types.Mixed,
    lyric: String,
}, { versionKey: false })

module.exports = model('Music', MusicSchema)